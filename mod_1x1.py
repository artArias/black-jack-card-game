import random
import numpy as np
import asyncio
import logging
import json
import contextlib
from aiohttp import web, websocket
# heart, diamond, club, spade
# ace, king, queen, jack


deck = ['6_h', '6_d', '6_c', '6_s',
        '7_h', '7_d', '7_c', '7_s',
        '8_h', '8_d', '8_c', '8_s',
        '9_h', '9_d', '9_c', '9_s',
        '10_h', '10_d', '10_c', '10_s',
        'jack_h', 'jack_d', 'jack_c', 'jack_s',
        'queen_h', 'queen_d', 'queen_c', 'queen_s',
        'king_h', 'king_d', 'king_c', 'king_s',
        'ace_h', 'ace_d', 'ace_c', 'ace_s']


accordance = {'6': 6, '7': 7, '8': 8, '9': 9, '1': 10, 'j': 2, 'q': 3, 'k': 4, 'a': 11}
results = []
gen_set =[] 
set_dealer =[]
point = [0, 0]


def deck_construction():
    random.shuffle(deck)


def is_card_needed(points, cards):
    number = np.random.uniform(0, 1)
    enough = 21 - points
    usefull = 0
    cards_balance = 36 - len(cards)
    virtual_deck = deck[::]
    for i in virtual_deck:
        if enough >= accordance[i[0]]:
            usefull += 1
    probability = usefull / cards_balance
    print("Prohibition is",probability,end="\n")
    if number >= probability:
        print("Not taken new card")
        return 0
    else:
        print("Taken new card")
        return 1


def points(cards):
    points = 0
    for i in cards:
        points += accordance[i[0]]
    if len(cards) == 2 and cards[0][0] == "a" and cards[1][0] == "a":
        return 21
    else:
        return points


def who_win(all_points):
    for i in range(len(all_points) - 1):
        player = all_points[i]
        dealer = all_points[-1]
        if player > dealer and player < 22 or dealer > 22 and player < 22:
            results.append("you win")
        elif all_points[i] == all_points[-1] or all_points[i] > 21 and all_points[-1] > 21:
            results.append("draw")
        else:
            results.append("dealer win")
    return


def dealer(socket):
    print("Dealer's set now", set_dealer, end="\n")
    point[1] = points(set_dealer)
    if is_card_needed(point[1], set_dealer):
        socket.send_str(json.dumps(deck[0]))
        set_dealer.append(deck[0])
        deck.remove(deck[0])
        print("Dealer's set after calculations", set_dealer, end="\n")
        if point[1] >= 21:
            who_win(point)
        else:
            dealer(socket)
    else:
        who_win(point)
    return


loop = asyncio.get_event_loop()
log = logging.getLogger("game")

GAMES={}

def start_round(clients):
    log.info("Round started")
    for i in range(len(clients)):
        clients[i].send_str(json.dumps(i))

async def echo_ws_handler(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    game_id = request.match_info["game_id"]
    if game_id not in GAMES:
        GAMES[game_id] = {"clients": [ws]} # start new game
        log.info("First player joined")
        start_round(GAMES[game_id]["clients"])

    socket = GAMES[game_id]["clients"][0]
    async for msg in ws:
        if msg.tp == websocket.MSG_TEXT:
            log.info("Received msg %s", str(msg))
            if msg.data == "go":
                deck_construction()
                distribution = deck[:4]
                for i in range(4):
                    if i < 2:
                        set_dealer.append(distribution[i])
                    else:
                        gen_set.append(distribution[i])
                    deck.remove(distribution[i])
                socket.send_str(json.dumps(gen_set))
                socket.send_str(json.dumps(set_dealer))
                point[0] = points(gen_set)
                if point[0] >= 21:
                    socket.send_str(json.dumps("waiting for the end"))
                else:
                    socket.send_str(json.dumps("WWAD")) # what we are doing
            if msg.data == "stop":
                socket.send_str(json.dumps("waiting for the end"))
            elif msg.data == "hit me":
                socket.send_str(json.dumps(deck[0]))
                gen_set.append(deck[0])
                deck.remove(deck[0])
                point[0] = points(gen_set)
                if point[0] >= 21:
                    socket.send_str(json.dumps("waiting for the end"))
                else:
                    socket.send_str(json.dumps("WWAD"))
            elif msg.data == "wait":
                dealer(socket)
                socket.send_str(json.dumps(results[0]))
        elif msg.tp == websocket.MSG_CLOSE:
            log.info("Error: %s", str(msg))
            break
        else:
            log.info("Strange msg: %s", str(msg))
        await asyncio.sleep(3.0)

    log.info("Closing websocket connection")
    return ws

async def start_server():
    app = web.Application()
    app.router.add_route("GET", "/game/{game_id}", echo_ws_handler)
    server = await loop.create_server(app.make_handler(), '0.0.0.0', 8081)
    return server

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, format="%(asctime)s [%(levelname)s]: %(message)s")
    log.info("Starting server")

    loop.run_until_complete(start_server())
    loop.run_forever()
